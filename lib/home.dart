import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ionicons/ionicons.dart';
import 'package:login/global.dart';
import 'package:geocoding/geocoding.dart';
import 'package:permission_handler/permission_handler.dart';
import 'main.dart';
import 'package:unique_identifier/unique_identifier.dart';
import 'package:mobile_number/mobile_number.dart';

void main() {
  runApp(Home(null, null));
}

class Home extends StatefulWidget {
  final context, string;
  Home(this.context, this.string);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  GoogleMapController? mapController;
  var currentLocation = "";
  var encryptedController = TextEditingController(text: "");
  var decryptedController = TextEditingController(text: "");
  var decryptEncrypt = true;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  /*
  true => decrypt
             |
          encrypt

  false => encrypt
             |
           decrypt
   */

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    if (!kIsWeb) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.location
      ].request();
      var currentPosition = await Global.getCurrentPosition(widget.string);
      print("CURRENT POSITION:");
      print(currentPosition);
      print("LATITUDE:");
      print(currentPosition.latitude);
      print("LONGITUDE:");
      print(currentPosition.longitude);
      List<Placemark> placemarks = await placemarkFromCoordinates(currentPosition.latitude, currentPosition.longitude);
      print("ALL PLACEMARKS:");
      print(placemarks);
      if (placemarks.length > 0) {
        setState(() {
          currentLocation = placemarks[0].street!+", "+placemarks[0].subLocality!+", "+placemarks[0].locality!+", "+placemarks[0].subAdministrativeArea!+", "+placemarks[0].administrativeArea!+", "+placemarks[0].country!;
        });
        mapController?.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              bearing: 270.0,
              target: LatLng(currentPosition.latitude, currentPosition.longitude),
              tilt: 30.0,
              zoom: 17.0
            )
          )
        );
        final MarkerId markerId = MarkerId("1");
        final Marker marker = Marker(
            markerId: markerId,
            position: LatLng(currentPosition.latitude, currentPosition.longitude),
            infoWindow: InfoWindow(title: "1", snippet: currentLocation),
            onTap: () {
            }
        );
        setState(() {
          markers[markerId] = marker;
        });
        String imei = (await UniqueIdentifier.serial)!;
        updateUserInfo(currentPosition, imei);
      }
    }
  }

  void updateUserInfo(currentPosition, imei) async {
    if (!await MobileNumber.hasPhonePermission) {
      await MobileNumber.requestPhonePermission;
      updateUserInfo(currentPosition, imei);
      return;
    }
    String mobileNumber = '';
    try {
      mobileNumber = (await MobileNumber.mobileNumber)!;
      var simCard = (await MobileNumber.getSimCards)!;
    } on PlatformException catch (e) {
      debugPrint("Failed to get mobile number because of '${e.message}'");
    }
    Global.httpPost(widget.string, Uri.parse(Global.API_URL+"/user/update_user_details"), body: <String, String>{
      "user_id": Global.USER_ID.toString(),
      "latitude": currentPosition.latitude.toString(),
      "longitude": currentPosition.longitude.toString(),
      "phone": mobileNumber,
      "imei": imei
    });
  }

  void convert() {
    var text = decryptEncrypt?decryptedController.text.trim():encryptedController.text.trim();
    if (text == "") {
      Global.show(widget.string.text18);
      return;
    }
    var result = "";
    if (decryptEncrypt) {
      result = Global.encrypt(text);
      encryptedController.text = result;
    } else {
      result = Global.decrypt(text);
      decryptedController.text = result;
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(body: SafeArea(
      child: Stack(
        children: [
          (() {
            if (kIsWeb) {
              return Container(width: width, height: height, margin: EdgeInsets.only(top: 83, bottom: 244), padding: EdgeInsets.only(left: 24, right: 24), color: Color(0xff130f40),
                  child: Center(child: Text(widget.string.text15, style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold), maxLines: 1)
              ));
            } else {
              return Padding(
                padding: EdgeInsets.only(top: 83, bottom: 244),
                child: GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                        target: LatLng(-6.2295712, 106.759478),
                        zoom: 14.4746
                    ),
                    onMapCreated: (GoogleMapController controller) {
                      setState(() {
                        mapController = controller;
                      });
                    },
					          markers: Set<Marker>.of(markers.values)
                )
              );
            }
          }()),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: width,
              height: 83,
              color: Colors.blue,
              padding: EdgeInsets.all(12),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(widget.string.text13, style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold)),
                          SizedBox(height: 2),
                          Text(currentLocation.trim()==""?widget.string.text14:currentLocation, style: TextStyle(color: Colors.white, fontSize: 13), maxLines: 2),
                        ]
                    )
                  ),
                  Container(width: 40, height: 50, child: GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      Global.showConfirmDialog(context, widget.string, widget.string.text12, () async {
                        Global.USER_ID = 0;
                        await Global.writeString("email", "");
                        await Global.writeString("password", "");
                        await Global.writeString("login_method", "email");
                        Global.replaceScreen(context, Main());
                      }, () {});
                    },
                    child: Center(child: Icon(Ionicons.exit, color: Colors.white, size: 22))
                  ))
                ]
              )
            )
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(width: width, height: 244, decoration: BoxDecoration(color: Colors.blue), child: Padding(
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(decryptEncrypt?widget.string.text16:widget.string.text19, style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold)),
                  SizedBox(height: 2),
                  Container(width: width-12-12, height: 40, decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(4)),
                    child: TextField(decoration: new InputDecoration(counterText: "", border: InputBorder.none, focusedBorder: InputBorder.none, enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none, disabledBorder: InputBorder.none, hintStyle: TextStyle(color: Color(0xFF9CA4AC), fontSize: 14),
                        contentPadding: EdgeInsets.only(left: 15, right: 15, bottom: 15)), textAlignVertical: TextAlignVertical.center,
                        controller: decryptEncrypt?decryptedController:encryptedController, keyboardType: TextInputType.text, style: TextStyle(fontSize: 14),
                    enabled: true)),
                  SizedBox(height: 8),
                  Align(alignment: Alignment.center, child: Container(width: 35, height: 35, decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(15)),
                      child: GestureDetector(behavior: HitTestBehavior.translucent, onTap: () {
                      setState(() {
                        decryptEncrypt = !decryptEncrypt;
                      });
                    },
                    child: Center(child: Icon(Ionicons.swap_vertical, color: Colors.white, size: 20))
                  ))),
                  Text(decryptEncrypt?widget.string.text17:widget.string.text20, style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold)),
                  SizedBox(height: 2),
                  Container(width: width-12-12, height: 40, decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(4)),
                    child: TextField(decoration: new InputDecoration(counterText: "", border: InputBorder.none, focusedBorder: InputBorder.none, enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none, disabledBorder: InputBorder.none, hintStyle: TextStyle(color: Color(0xFF9CA4AC), fontSize: 14),
                        contentPadding: EdgeInsets.only(left: 15, right: 15, bottom: 15)), textAlignVertical: TextAlignVertical.center,
                        controller: decryptEncrypt?encryptedController:decryptedController, keyboardType: TextInputType.text, style: TextStyle(fontSize: 14),
                        enabled: false)),
                  SizedBox(height: 8),
                  Container(width: width-12-12, height: 45, decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(4)), child: GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      convert();
                    },
                    child: Center(child: Text(widget.string.convert, style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold)))
                  ))
                ]
              )
            ))
          )
        ]
      )
    ));
  }
}
