import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'global.dart';
import 'home.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:twitter_login/twitter_login.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
	clientId: "317017105235-tav1r028iqll7oqe08dr0aefkib7qdr3.apps.googleusercontent.com",
	scopes: <String>[
		'email',
		'https://www.googleapis.com/auth/contacts.readonly',
	],
);

void main() {
  runApp(Login(null, null));
}

class Login extends StatefulWidget {
  final context, string;
  Login(this.context, this.string);

  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> {
	var emailController = TextEditingController(text: "");
	var passwordController = TextEditingController(text: "");
	var passwordShown = false;
	var isFacebookLoggedIn = false;

	@override
  void initState() {
    super.initState();
  }

  void login(context, string) async {
  	var email = emailController.text.trim();
		var password = passwordController.text;
		if (email == "" || password.trim() == "") {
			Global.show(string.text3);
			return;
		}
		if (!email.contains("@") || !email.contains(".")) {
			Global.show(string.text4);
			return;
		}
		if (email.contains("@.")) {
			Global.show(string.text4);
			return;
		}
		var pd = await Global.showProgressDialog(context, widget.string.text5);
		Global.httpPost(string, Uri.parse(Global.API_URL+"/user/login"), body: <String, String>{
			"email": email,
			"password": password
		}, onSuccess: (response) async {
			var obj = jsonDecode(response);
			await Global.hideProgressDialog(pd);
			var responseCode = int.parse(obj['response_code'].toString());
			if (responseCode == 1) {
				Global.USER_ID = int.parse(obj['user_id'].toString());
				await Global.writeString("email", email);
				await Global.writeString("password", password);
				await Global.writeString("login_method", "email");
				Global.replaceScreen(context, Home(context, string));
			} else if (responseCode == -1) {
				Global.show(string.text6);
			} else if (responseCode == -2) {
				Global.show(string.text7);
			}
		});
	}

	void onLoginStatusChanged(bool isLoggedIn) {
		setState(() {
			isFacebookLoggedIn = isLoggedIn;
		});
	}

  @override
  Widget build(BuildContext context) {
	var width = MediaQuery.of(context).size.width;
	var height = MediaQuery.of(context).size.height;
    return Scaffold(body: SafeArea(
	  child: Stack(
	    children: [
		  Image.asset("assets/images/login_bg.jpg", width: width, height: height, fit: BoxFit.cover),
		  Center(
		    child: Container(width: width-32-32, height: 518, decoration: BoxDecoration(color: Color(0x7FFFFFFF), borderRadius: BorderRadius.circular(10)), child: Padding(
			  padding: EdgeInsets.all(16),
			  child: Column(
			    crossAxisAlignment: CrossAxisAlignment.start,
				children: [
				  SizedBox(height: 12),
				  Align(alignment: Alignment.topCenter, child: Text(widget.string.login, style: TextStyle(color: Colors.white, fontSize: 20))),
				  Text(widget.string.email, style: TextStyle(color: Colors.white, fontSize: 13)),
				  SizedBox(height: 4),
				  Container(height: 40, decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(4)),
							child: TextField(decoration: new InputDecoration(counterText: "", border: InputBorder.none, focusedBorder: InputBorder.none, enabledBorder: InputBorder.none,
									errorBorder: InputBorder.none, disabledBorder: InputBorder.none, hintStyle: TextStyle(color: Color(0xFF9CA4AC), fontSize: 14),
									contentPadding: EdgeInsets.only(left: 15, right: 15, bottom: 15)), textAlignVertical: TextAlignVertical.center, controller: emailController,
									keyboardType: TextInputType.emailAddress, style: TextStyle(fontSize: 14))),
				  SizedBox(height: 12),
				  Text(widget.string.password, style: TextStyle(color: Colors.white, fontSize: 13)),
				  SizedBox(height: 4),
					Container(height: 40, decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(4)),
							child: TextField(decoration: new InputDecoration(counterText: "", border: InputBorder.none, focusedBorder: InputBorder.none, enabledBorder: InputBorder.none,
									errorBorder: InputBorder.none, disabledBorder: InputBorder.none, hintStyle: TextStyle(color: Color(0xFF9CA4AC), fontSize: 14),
									contentPadding: EdgeInsets.only(left: 15, right: 15, bottom: 15)), textAlignVertical: TextAlignVertical.center, controller: passwordController,
									keyboardType: TextInputType.text, style: TextStyle(fontSize: 14), obscureText: passwordShown?false:true)),
				  SizedBox(height: 20),
				  Container(height: 40, decoration: BoxDecoration(gradient:  LinearGradient(
						begin: Alignment.topLeft,
						end: Alignment(0.8, 0.0),
						colors: <Color>[
							Color(0xff149271),
							Color(0xff23c89a)
						]
					), color: Colors.blue, borderRadius: BorderRadius.circular(4)), child: GestureDetector(
				    behavior: HitTestBehavior.translucent,
					onTap: () {
				    	login(context, widget.string);
					},
					child: Center(child: Text(widget.string.login, style: TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold)))
				  )),
				  SizedBox(height: 20),
				  Row(
				    children: [
					  Expanded(child: Container(height: 0.5, color: Colors.white)),
					  SizedBox(width: 4),
					  Text(widget.string.text1, style: TextStyle(color: Colors.white, fontSize: 12)),
					  SizedBox(width: 4),
					  Expanded(child: Container(height: 0.5, color: Colors.white))
					]
				  ),
				  SizedBox(height: 20),
				  Align(alignment: Alignment.center, child: Container(width: width-32-32-16-16, height: 40, child: SignInButton(Buttons.Google, onPressed: () async {
						try {
							Global.show("Signing in...");
							GoogleSignInAccount account = (await _googleSignIn.signIn())!;
							Global.show("SIGNED ACCOUNT: "+account.email);
							var pd = await Global.showProgressDialog(context, widget.string.text21);
							Global.httpPost(widget.string, Uri.parse(Global.API_URL+"/user/login_with_google"), body: <String, String>{
								"email": account.email,
								"display_name": account.displayName!
							}, onSuccess: (response) async {
								print("LOGIN WITH GOOGLE RESPONSE:");
								print(response);
								Global.show(response);
								var obj = jsonDecode(response);
								var userID = int.parse(obj['user_id'].toString());
								Global.USER_ID = userID;
								await Global.writeString("email", account.email);
								await Global.writeString("display_name", account.displayName!);
								await Global.writeString("login_method", "google");
								await Global.hideProgressDialog(pd);
								Global.replaceScreen(context, Home(context, widget.string));
							});
						} catch (error) {
							print(error);
							Global.show(error.toString());
						}
				  }))),
				  SizedBox(height: 8),
				  Align(alignment: Alignment.center, child: Container(width: width-32-32-16-16, height: 40, child: SignInButton(Buttons.Facebook, onPressed: () async {
						final LoginResult result = await FacebookAuth.instance.login();
						if (result.status == LoginStatus.success) {
							final userData = await FacebookAuth.i.getUserData(
								fields: "name,email",
							);
							print("USER DETAILS:");
							print(userData);
							var pd = await Global.showProgressDialog(context, widget.string.text21);
							Global.httpPost(widget.string, Uri.parse(Global.API_URL+"/user/login_with_facebook"), body: <String, String>{
								"email": userData['email'],
								"display_name": userData['name']
							}, onSuccess: (response) async {
								print("LOGIN WITH FACEBOOK RESPONSE:");
								print(response);
								Global.show(response);
								var obj = jsonDecode(response);
								var userID = int.parse(obj['user_id'].toString());
								Global.USER_ID = userID;
								await Global.writeString("email", userData['email']);
								await Global.writeString("display_name", userData['name']);
								await Global.writeString("login_method", "facebook");
								await Global.hideProgressDialog(pd);
								Global.replaceScreen(context, Home(context, widget.string));
							});
						}
				  }))),
				  SizedBox(height: 20),
				  Align(alignment: Alignment.center, child: Text(widget.string.text2, style: TextStyle(color: Colors.white, fontSize: 13))),
				  SizedBox(height: 10)
				]
			  )
			))
		  )
		]
	  )
	));
  }
}
