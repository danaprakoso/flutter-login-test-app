import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as Encryptor;

class Global {
	static const TIMEOUT = 60;
  static const PROTOCOL = "https";
  static const HOST = "danaos.xyz";
  static const API_URL = PROTOCOL+"://"+HOST+"/flutterapi";
  static const USERDATA_URL = PROTOCOL+"://"+HOST+"/flutterapi/userdata/";
  static var USER_ID = 0;
	
	static void show(message) {
		Fluttertoast.showToast(
		  msg: message,
          toastLength: Toast.LENGTH_LONG,
		  gravity: ToastGravity.BOTTOM
        );
	}
	
	static Future<ProgressDialog> showProgressDialog(context, message) async {
		final ProgressDialog pd = ProgressDialog(context);
		pd.style(message: message);
		await pd.show();
		return pd;
	}
	
	static Future<void> hideProgressDialog(pd) async {
		await pd.hide();
	}
	
	static void httpGet(string, uri, {var headers = null, onSuccess = null, onError = null, onTimeout = null}) async {
    if (headers == null) {
      headers = Map<String, String>();
    }
    var response = await http.get(uri, headers: headers).timeout(
  	  Duration(seconds: TIMEOUT),
  	  onTimeout: () {
  	    if (string == null) {
          Global.show("Batas maksimum koneksi terlampaui");
        } else {
          Global.show(string.text265);
        }
  	    if (onTimeout != null) {
          onTimeout();
        }
    	  return http.Response('Error', 500);
  	  }
	  );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      if (onSuccess != null) {
        onSuccess(response.body);
      }
    } else {
      if (response.statusCode == 500) {
        return;
      }
      Global.show(string.text266+" ["+response.statusCode.toString()+"]");
      if (onError != null) {
        onError();
      }
    }
  }

  static httpGetSync(string, uri, {var headers = null}) async {
    if (headers == null) {
      headers = Map<String, String>();
    }
    var response = await http.get(uri, headers: headers).timeout(
        Duration(seconds: TIMEOUT),
        onTimeout: () {
          Global.show(string.text265);
          return http.Response('Error', 500);
        }
    );
    return response;
  }

  static void httpPost(string, uri, {var body = null, var headers = null, onSuccess = null, onError = null, onTimeout = null}) async {
    if (headers == null) {
      headers = Map<String, String>();
    }
    var response = await http.post(uri, headers: headers, body: body).timeout(
  	  Duration(seconds: TIMEOUT),
  	  onTimeout: () {
        Global.show(string.text265);
        if (onTimeout != null) {
          onTimeout();
        }
    	  return http.Response('Error', 500);
  	  },
	  );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      if (onSuccess != null) {
        onSuccess(response.body);
      }
    } else {
      if (response.statusCode == 500) {
        return;
      }
      Global.show(string.text266+" ["+response.statusCode.toString()+"]");
      if (onError != null) {
        onError();
      }
    }
  }

  static httpPostSync(string, uri, {var body = null, var headers = null}) async {
    if (headers == null) {
      headers = Map<String, String>();
    }
    var response = await http.post(uri, headers: headers, body: body).timeout(
      Duration(seconds: TIMEOUT),
      onTimeout: () {
        Global.show(string.text265);
        return http.Response('Error', 500);
      },
    );
    return response;
  }
  
  static void navigate(context, screen) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => screen));
  }
  
  static void replaceScreen(context, screen) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => screen));
  }

  static Future<String> readString(name, defaultValue) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(name)??defaultValue;
  }

  static Future<int> readInt(name, defaultValue) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt(name)??defaultValue;
  }

  static Future<bool> readBool(name, defaultValue) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(name)??defaultValue;
  }

  static Future<double> readDouble(name, defaultValue) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getDouble(name)??defaultValue;
  }

  static Future<void> writeString(name, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(name, value);
  }

  static Future<void> writeInt(name, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(name, value);
  }

  static Future<void> writeDouble(name, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble(name, value);
  }

  static Future<void> writeBool(name, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(name, value);
  }

  static Future<Position> getCurrentPosition(string) async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await Geolocator.openLocationSettings();
      return Future.error(string.text8);
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error(string.text9);
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(string.text11);
    }
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  static void showConfirmDialog(context, string, message, onOk, onCancel) {
    AlertDialog alert = AlertDialog(
      title: Text(string.confirmation),
      content: Text(message),
      actions: [
        TextButton(
          child: Text(string.yes),
          onPressed: () {
            Navigator.pop(context);
            if (onOk != null) {
              onOk();
            }
          },
        ),
        TextButton(
          child: Text(string.no),
          onPressed: () {
            Navigator.pop(context);
            if (onCancel != null) {
              onCancel();
            }
          },
        ),
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      }
    );
  }

  static String encrypt(message) {
    final key = Encryptor.Key.fromLength(32);
    final iv = Encryptor.IV.fromLength(16);
    final encrypter = Encryptor.Encrypter(Encryptor.AES(key));
    final encrypted = encrypter.encrypt(message, iv: iv);
    final decrypted = encrypter.decrypt(encrypted, iv: iv);
    return encrypted.base64;
  }

  static String decrypt(message) {
    final key = Encryptor.Key.fromLength(32);
    final iv = Encryptor.IV.fromLength(16);
    final encrypter = Encryptor.Encrypter(Encryptor.AES(key));
    return encrypter.decrypt64(message, iv: iv);
  }
}