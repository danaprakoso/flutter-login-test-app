import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'global.dart';
import 'home.dart';
import 'login.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        title: "Login Test",
        theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'Poppins'),
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [Locale('en', '')],
        home: Main());
  }
}

class Main extends StatefulWidget {

  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {

  @override
  void initState() {
    super.initState();
  }

  Future<String> initData(context, string) async {
	  WidgetsBinding.instance!.addPostFrameCallback((_) async {
	    var email = await Global.readString("email", "");
      var password = await Global.readString("password", "");
      var loginMethod = await Global.readString("login_method", "");
      if (email != null && password != null && loginMethod != null && email.trim() != "" && password.trim() != "" && loginMethod.trim() != "") {
        if (loginMethod == "email") {
          var pd = await Global.showProgressDialog(context, string.text5);
          Global.httpPost(string, Uri.parse(Global.API_URL + "/user/login"),
              body: <String, String>{
                "email": email,
                "password": password
              }, onSuccess: (response) async {
                var obj = jsonDecode(response);
                await Global.hideProgressDialog(pd);
                var responseCode = int.parse(obj['response_code'].toString());
                if (responseCode == 1) {
                  Global.USER_ID = int.parse(obj['user_id'].toString());
                  Global.replaceScreen(context, Home(context, string));
                } else if (responseCode == -1) {
                  Global.replaceScreen(context, Login(context, string));
                } else if (responseCode == -2) {
                  Global.replaceScreen(context, Login(context, string));
                }
              });
        } else if (loginMethod == "google") {
          var pd = await Global.showProgressDialog(context, string.text21);
          Global.httpPost(string, Uri.parse(Global.API_URL+"/user/login_with_google"), body: <String, String>{
            "email": email,
            "display_name": await Global.readString("display_name", "")
          }, onSuccess: (response) async {
            var obj = jsonDecode(response);
            var userID = int.parse(obj['user_id'].toString());
            Global.USER_ID = userID;
            await Global.hideProgressDialog(pd);
            Global.replaceScreen(context, Home(context, string));
          });
        } else if (loginMethod == "facebook") {
          var pd = await Global.showProgressDialog(context, string.text21);
          Global.httpPost(string, Uri.parse(Global.API_URL+"/user/login_with_facebook"), body: <String, String>{
            "email": email,
            "display_name": await Global.readString("display_name", "")
          }, onSuccess: (response) async {
            var obj = jsonDecode(response);
            var userID = int.parse(obj['user_id'].toString());
            Global.USER_ID = userID;
            await Global.hideProgressDialog(pd);
            Global.replaceScreen(context, Home(context, string));
          });
        }
      } else {
        Global.replaceScreen(context, Login(context, string));
      }
	  });
	  return "";
  }

  @override
  Widget build(BuildContext context) {
	  var string = AppLocalizations.of(context);
    return Scaffold(
	  body: FutureBuilder<String>(
	    future: initData(context, string),
		builder: (context, snapshot) {
			return Container();
		}
	  )
	);
  }
}
